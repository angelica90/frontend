import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { menu } from './side-menu/menu.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'proyecta';
  sidemenu : menu; //Tipo menú

  constructor(private http : HttpClient) {}

  ngOnInit(){
    this.fetchData();
  }

  private fetchData(){
    this.http.get<menu>('http://localhost:7431/menu')
    .subscribe( responseData => {
      this.sidemenu = responseData;
    } );
    /*  TODO: Obtener información get de localhost:7431/home  */
  }
}
